﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollowsPlayer : MonoBehaviour
{
    private List<Rigidbody2D> enemyRBs;
    public float speed = 4f;
    private Transform playerPosition;
    private Rigidbody2D rb;
    private float repelRange = 0.5f;

    void Awake() {
        playerPosition = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        if (enemyRBs == null) { enemyRBs = new List<Rigidbody2D>(); }
        enemyRBs.Add(rb);
    }

    void OnDestroy() { enemyRBs.Remove(rb); }

    // Update is called once per frame
    void Update() {
        if (Vector2.Distance(transform.position, playerPosition.position) > 0.25f) {
            transform.position = Vector2.MoveTowards(
                transform.position,
                playerPosition.position,
                speed * Time.deltaTime
            );
        }
    }

    void FixedUpdate() {
        Vector2 repelForce = Vector2.zero;

        foreach(Rigidbody2D enemy in enemyRBs) {
            if (enemy == rb) { continue; }

            if (Vector2.Distance(enemy.position, rb.position) <= repelRange) {
                Vector2 repelDirection = (rb.position - enemy.position).normalized;
                repelForce += repelDirection;
            }
        }
    }
}
