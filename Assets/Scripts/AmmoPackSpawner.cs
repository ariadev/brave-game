﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AmmoPackSpawner : MonoBehaviour
{
    [SerializeField] private float spawnRadius, time = 3f;
    public GameObject ammoPack;

    void Update() {
        int bullets = GameObject.Find("Player").GetComponent<PlayerController>().bullets;
        if (bullets <= 5) { StartCoroutine(SpawnAmmoPack()); }
    }

    IEnumerator SpawnAmmoPack() {
        Vector2 spawnPosition = GameObject.Find("Player").transform.position;
        spawnPosition += Random.insideUnitCircle.normalized * spawnRadius;

        if (GameObject.FindGameObjectsWithTag("AmmoPack").Length < 1) {
            Instantiate(ammoPack, spawnPosition, Quaternion.identity);
        }
        
        yield return new WaitForSeconds(time);
    }
}
