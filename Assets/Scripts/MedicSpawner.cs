﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicSpawner : MonoBehaviour
{
    [SerializeField] private float spawnRadius, time = 3f;
    public GameObject medicPack;

    void Update() {
        int health = GameObject.Find("Player").GetComponent<PlayerController>().health;
        if (health <= 2) { StartCoroutine(SpawnMedicPack()); }
    }

    IEnumerator SpawnMedicPack() {
        Vector2 spawnPosition = GameObject.FindWithTag("Player").transform.position;
        spawnPosition += Random.insideUnitCircle.normalized * spawnRadius;

        if (GameObject.FindGameObjectsWithTag("Medic").Length < 1) {
            Instantiate(medicPack, spawnPosition, Quaternion.identity);
        }
        
        yield return new WaitForSeconds(time);
    }
}
