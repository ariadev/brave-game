﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleBullet : MonoBehaviour
{
    private float speed = 6f;
    private Vector2 direction;
    public GameObject tombstone;
    public AudioClip zombieDying;

    // Start is called before the first frame update
    void Start() {
        direction = GameObject.Find("Direction").transform.position;
        transform.position = GameObject.Find("FirePoint").transform.position;
        StartCoroutine(DestroyBullet());
    }

    // Update is called once per frame
    void Update() {
        transform.position = Vector2.MoveTowards(transform.position, direction, speed * Time.deltaTime);

        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.y > Screen.height || screenPosition.y < 0) {
            Destroy(this.gameObject);
        }

        if (screenPosition.x > Screen.width || screenPosition.x < 0) {
            Destroy(this.gameObject);
        }
    }

    IEnumerator DestroyBullet() { yield return new WaitForSeconds(2f); Destroy(gameObject); }
    

    void OnTriggerEnter2D(Collider2D target) {
        if (target.CompareTag("Enemy")) {
            AudioSource zombieAudioSource = GameObject.FindWithTag("Player").GetComponent<AudioSource>();
            zombieAudioSource.PlayOneShot(zombieDying, 0.5f);

            GameObject.FindWithTag("Player").GetComponent<PlayerController>().IncreaseScore(5);

            Destroy(target.gameObject);
            Destroy(gameObject);
            Instantiate(tombstone, target.gameObject.transform.position, Quaternion.identity);
        }
    }
}
