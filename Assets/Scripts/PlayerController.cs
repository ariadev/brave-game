﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public GameObject bullet;
    private Rigidbody2D playerBody;
    private Animator anim;
    private Animator legAnimation;
    public Animator scoreAnimation;
    public float speed = 2f;
    private Vector2 moveVelocity;
    [SerializeField]
    public int health = 7;

    public GameObject healthSliece;
    private bool hit = true;
    public int bullets = 10;
    public AudioSource gunShotAudioSource;
    public AudioClip gunShotAudioClip;
    public AudioClip gunEmptyAudioClip;
    public AudioClip gunReloadAudioClip;
    public AudioClip EnemyHitsThePlayerAudioClip;
    public AudioClip MedicAudioClip;
    public float soundEffectValume = 0.5f;
    public GameObject ammoBox;
    public GameObject ammoBoxBullet;

    public int score = 0;
    public Text scoreHandler;

    void Awake() {
        playerBody = GetComponent<Rigidbody2D>();
        legAnimation = transform.GetChild(1).GetComponent<Animator>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        Vector2 playerDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(playerDirection.y, playerDirection.x) * Mathf.Rad2Deg + 90;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 10 * Time.deltaTime);

        if (Input.GetMouseButtonDown(0)) {
            if (bullets > 0) {
                bullets--;
                gunShotAudioSource.PlayOneShot(gunShotAudioClip, soundEffectValume);
                Instantiate(bullet, transform.position, Quaternion.identity);
                Destroy(GameObject.Find("AmmoBox").transform.GetChild(0).gameObject);   
            } else {
                gunShotAudioSource.PlayOneShot(gunEmptyAudioClip, soundEffectValume);
            }
        }
        
        transform.position = new Vector2(
            Mathf.Clamp(transform.position.x, -8.5f, 8.5f), 
            Mathf.Clamp(transform.position.y, -4.5f, 4.5f)
            );

        if (health == 0)
        {
            SceneManager.LoadScene("Menu");
            Time.timeScale = 1f;
        }
    }

    void FixedUpdate() { Movement(); }

    void Movement() {
        Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * speed;
        playerBody.MovePosition(playerBody.position + moveVelocity * Time.fixedDeltaTime);

        if (moveVelocity == Vector2.zero) { legAnimation.SetBool("Moving", false); } 
        else { legAnimation.SetBool("Moving", true); }
    }

    IEnumerator HitBoxOff()
    {
        health--;
        Destroy(GameObject.Find("LifeBox").transform.GetChild(0).gameObject);
        gunShotAudioSource.PlayOneShot(EnemyHitsThePlayerAudioClip, soundEffectValume);
        hit = false; anim.SetTrigger("Hit"); yield return new WaitForSeconds(1f); hit = true;
    }

    public void IncreaseScore(int enemyScore) {
        score += enemyScore; 
        scoreHandler.text = score.ToString();
        scoreAnimation = GameObject.Find("ScoreLabel").GetComponent<Animator>();
        
        scoreAnimation.Play("");
    }
    
    void OnTriggerStay2D(Collider2D target) {
        if (target.CompareTag("Enemy")) {
            if (hit) { StartCoroutine(HitBoxOff()); }
        }

        if (target.CompareTag("AmmoPack")) {
            gunShotAudioSource.PlayOneShot(gunReloadAudioClip, soundEffectValume);
            for (int i = ammoBox.transform.childCount; i < 10; i++) {
                GameObject ammoBullet = Instantiate(ammoBoxBullet);
                ammoBullet.transform.SetParent(ammoBox.transform, false);
            }
            bullets = 10;
            Destroy(target.gameObject);
        }

        if (target.CompareTag("Medic")) {
            gunShotAudioSource.PlayOneShot(MedicAudioClip, soundEffectValume);
            for (int i = GameObject.Find("LifeBox").transform.childCount; i < 7; i++) {
                GameObject healthPeace = Instantiate(healthSliece);
                healthPeace.transform.SetParent(GameObject.Find("LifeBox").transform, false);
            }

            health = 7;
            Destroy(target.gameObject);
        }
    }
}
