﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private float spawnRadius = 7, time = 1.5f;
    public GameObject[] enemies;

    // Start is called before the first frame update
    void Start() { StartCoroutine(SpawnAnEnemy()); }

    IEnumerator SpawnAnEnemy() {
        Vector2 spawnPosition = GameObject.Find("Player").transform.position;
        spawnPosition += Random.insideUnitCircle.normalized * spawnRadius;

        if (GameObject.FindGameObjectsWithTag("Enemy").Length > 5) { time = 0.8f; }

        // If enemies over, don't spawn anymore to increase usage of RAM
        if (GameObject.FindGameObjectsWithTag("Enemy").Length <= 20) {
            Instantiate(enemies[Random.Range(0, enemies.Length)], spawnPosition, Quaternion.identity);
            yield return new WaitForSeconds(time);
        }

        StartCoroutine(SpawnAnEnemy());
    }
}
