﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPackController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start() { StartCoroutine(DestroyAmmoPackAfterSeconds()); }

    IEnumerator DestroyAmmoPackAfterSeconds() { yield return new WaitForSeconds(5f); Destroy(gameObject); }
}
