﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform playerPosition;

    // Start is called before the first frame update
    void Awake() { playerPosition = GameObject.FindGameObjectWithTag("Player").transform; }

    // Update is called once per frame
    void Update() {
        transform.position = new Vector3(
            playerPosition.position.x, 
            playerPosition.position.y, 
            transform.position.z);
        
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, -4.5f, 4.5f), 
            Mathf.Clamp(transform.position.y, -2.4f, 2.4f),
            transform.position.z);
    }
}
