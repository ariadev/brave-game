﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TombstoneController : MonoBehaviour
{
    private float time = 2f;
    
    // Start is called before the first frame update
    void Start() { StartCoroutine(DestoryTombstone()); }

    IEnumerator DestoryTombstone() { yield return new WaitForSeconds(time); Destroy(this.gameObject); }
}
